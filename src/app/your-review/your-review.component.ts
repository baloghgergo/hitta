import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {Router} from '@angular/router';
import {debug} from 'util';

@Component({
  selector: 'app-your-review',
  templateUrl: './your-review.component.html',
  styleUrls: ['./your-review.component.scss']
})
export class YourReviewComponent implements OnInit {
  $ratings: object;
  $rating: 0;
  $name: string;
  $anonym = 'Anonymous';
  $label: '';
  $selectedRating: object;
  $time = '5 min ago';
  $review: string;

  constructor(private data: DataService, private router: Router) {
  }

  ngOnInit() {
    this.data.currentRating.subscribe(ratings => {
      this.$ratings = ratings;
    });
    this.data.currentName.subscribe(name => this.$name = name);
    this.data.currentSelectedRating.subscribe(selectedRating => {
      this.$selectedRating = selectedRating;
      this.$label = this.$selectedRating['label'];
      this.$rating = this.$selectedRating['value'];
    });
    this.data.currentReview.subscribe(review => this.$review = review);
  }

  onChangeStars(e) {
    for (const key of Object.keys(this.$ratings)) {
      if (this.$ratings[key].value === e.rating) {
        this.$selectedRating = this.$ratings[key];
      }
    }
    this.$label = this.$selectedRating['label'];
    this.$rating = this.$selectedRating['value'];
    this.data.changeRating(this.$selectedRating);
    this.router.navigate(['/review']);
  }
}
