import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StarRatingModule } from 'angular-star-rating';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { ReviewsHeaderComponent } from './reviews-header/reviews-header.component';
import { YourReviewComponent } from './your-review/your-review.component';
import { LatestReviewsComponent } from './latest-reviews/latest-reviews.component';
import { ReviewComponent } from './review/review.component';
import { ReviewPageComponent } from './review-page/review-page.component';
import { CompanyPageComponent } from './company-page/company-page.component';
import { DataService } from './data.service';
import { MyModalComponent } from './my-modal/my-modal.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ReviewsHeaderComponent,
    YourReviewComponent,
    LatestReviewsComponent,
    ReviewComponent,
    ReviewPageComponent,
    CompanyPageComponent,
    MyModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    StarRatingModule.forRoot(),
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
  entryComponents: [MyModalComponent],
})
export class AppModule { }
