import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReviewPageComponent} from './review-page/review-page.component';
import {CompanyPageComponent} from './company-page/company-page.component';

const routes: Routes = [
  {path: '', component: CompanyPageComponent},
  {path: 'review', component: ReviewPageComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
