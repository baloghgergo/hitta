import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatToolbarModule,
  MatDividerModule,
  MatGridListModule,
  MatInputModule,
  MatIconModule,
  MatDialogModule

} from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatToolbarModule, MatDividerModule, MatGridListModule, MatInputModule, MatIconModule, MatDialogModule],
  exports: [MatButtonModule, MatToolbarModule, MatDividerModule, MatGridListModule, MatInputModule, MatIconModule, MatDialogModule],
})
export class MaterialModule {
}
