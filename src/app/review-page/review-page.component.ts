import {Component, OnInit, Inject, ɵEMPTY_ARRAY} from '@angular/core';
import {DataService} from '../data.service';
import {MyModalComponent} from '../my-modal/my-modal.component';
import {MatDialog, MatDialogConfig} from '@angular/material';

@Component({
  selector: 'app-review-page',
  templateUrl: './review-page.component.html',
  styleUrls: ['./review-page.component.scss']
})
export class ReviewPageComponent implements OnInit {
  $ratings: object;
  $selectedRating: object;
  $label: string;
  $name: string;
  $rating: string;
  $review: string;

  constructor(private data: DataService, public dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MyModalComponent, {
      data: {$name: this.$name},
      panelClass: 'myClass'
    });
    if (this.$name === '') {
      this.$name = 'Anonymous';
    }
    this.data.addName(this.$name);
    this.data.addReview(this.$review);
    this.data.changeRating(this.$selectedRating);
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
    const myRating = [];
    myRating.push(this.$rating, this.$name, this.$review);
    this.data.saveRating(myRating);
  }

  ngOnInit() {
    this.data.currentName.subscribe(name => this.$name = name);
    this.data.currentRating.subscribe(rating => {
      this.$ratings = rating;
    });
    this.data.currentSelectedRating.subscribe(selectedRating => {
      this.$selectedRating = selectedRating;
    });
    this.$rating = this.$selectedRating['value'];
    this.$label = this.$selectedRating['label'];
    this.data.currentReview.subscribe(review => this.$review = review);
  }

  onChangeStars(e) {
    for (const key of Object.keys(this.$ratings)) {
      if (this.$ratings[key].value === e.rating) {

        this.$selectedRating = this.$ratings[key];
      }
    }
    this.data.changeRating(this.$selectedRating);
  }

  addName(e) {
    this.$name = e.target.value;
  }

  addReview(e) {
    this.$review = e.target.value;
  }
}
