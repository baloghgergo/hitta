import {Injectable} from '@angular/core';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import {BoundEventAst} from '@angular/compiler';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class DataService {

  private ratingSource = new BehaviorSubject([{label: 'I hated it', value: 1}, {
    label:
      `I didn't like it`, value: 2
  }, {label: 'It was ok', value: 3}, {label: 'I liked it', value: 4}, {label: 'I loved it', value: 5}]);
  currentRating = this.ratingSource.asObservable();
  private nameSource = new BehaviorSubject('');
  currentName = this.nameSource.asObservable();
  private selectedRatingSource = new ReplaySubject();
  currentSelectedRating = this.selectedRatingSource.asObservable();
  private reviewSource = new BehaviorSubject('');
  currentReview = this.reviewSource.asObservable();

  constructor(private http: HttpClient) {
  }

  changeRating(rating) {
    this.selectedRatingSource.next(rating);
  }

  addName(name: string) {
    this.nameSource.next(name);
  }

  addReview(review: string) {
    this.reviewSource.next(review);
  }
  //
  // getRatingFromObject() {
  //
  // }
  //
  // getLabelFromObject() {
  //
  // }
  saveRating(rating) {
    const [score, name, comment] = rating;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        'X-HITTA-DEVICE-NAME': 'MOBILE_WEB',
        'X-HITTA-SHARED-IDENTIFIER': '15188693697264027'
      })
    };
    const url = 'https://test.hitta.se/reviews/v1/company';
    const postRating = {
      'score': score,
      'companyId': 'VdgWZZ___C',
      'comment': comment,
      'userName': name,
    };
    return this.http.post(url, JSON.stringify(postRating), httpOptions).subscribe(res => console.log(res));
  }
}
